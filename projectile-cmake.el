;;; projectile-cmake.el --- projectile integration for cmake

;; Author: John Eivind Helset <jehelset@gmail.com>
;; URL: https://gitlab.com/jehelset/projectile-cmake
;; Keywords: project, cmake
;; Version: 0.0.0-1
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

(require 'json)
(require 'compile)
(require 'projectile)

(defun projectile-cmake--version()
  "Compute CMake version."
  (let* ((string (shell-command-to-string "cmake --version"))
         (match (string-match "^cmake version \\([0-9]*\\)\.\\([0-9]*\\)" string)))
    (when match
      (mapcar 'string-to-number
              (list (match-string 1 string)
                    (match-string 2 string))))))

(defun projectile-cmake--presets-supported()
  "Check if CMake supports presets."
  (version-list-<= '(3 19) (projectile-cmake--version)))

(defun projectile-cmake--read-presets-json(filename)
  "Read CMake presets."
  (when (file-exists-p filename)
        (with-temp-buffer
          (insert-file-contents filename)
          (json-parse-string (buffer-string)))))

(defun projectile-cmake--read-configure-presets(filename)
  "Get CMake presets from FILENAME."
  (when-let ((presets (projectile-cmake--read-presets-json filename)))
    (gethash "configurePresets" presets)))

(defun projectile-cmake--user-presets-file()
  "Compute file to CMake user presets file"
  (concat (file-name-as-directory (projectile-project-root))
          "CMakeUserPresets.json"))

(defun projectile-cmake--system-presets-file()
  "Compute file to CMake system presets file"
  (concat (file-name-as-directory (projectile-project-root))
          "CMakeSystemPresets.json"))                                  

(defun projectile-cmake--user-configure-presets()
  "Get CMake user-presets"
  (projectile-cmake--read-configure-presets (projectile-cmake--user-presets-file)))

(defun projectile-cmake--system-configure-presets()
  "Get CMake system-presets"
  (projectile-cmake--read-configure-presets (projectile-cmake--system-presets-file)))

(defun projectile-cmake--configure-presets()
  "Get CMake presets"
  (append
   (projectile-cmake--user-configure-presets)
   (projectile-cmake--system-configure-presets)))

(defconst projectile-cmake--no-preset "*no preset*")

(defun projectile-cmake--select-configure-preset()
  "Select CMake preset"
  (let* ((configure-preset-names
          (append `(,projectile-cmake--no-preset)
                  (mapcar (lambda (configure-preset)
                            (gethash "name" configure-preset))
                          (projectile-cmake--configure-presets)))))
    (projectile-completing-read "Use preset: "
                                configure-preset-names)))

;;;###autoload
(defun projectile-cmake-configure-cmd()
  "Select CMake preset"
  (interactive)
  (let ((maybe-preset (projectile-cmake--select-configure-preset)))
    (if (equal maybe-preset projectile-cmake--no-preset)
        "cmake %s -B %s"
      (compile
         (format "cmake %s --preset=%s"
                 (projectile-project-root)
                 maybe-preset)))))

;;;###autoload
(defun projectile-cmake-enable()
  "Override projectile commands for CMake"
  (plist-put (alist-get 'cmake projectile-project-types) 'configure-command 'projectile-cmake-configure-cmd))

(provide 'projectile-cmake)
